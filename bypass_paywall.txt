! Title: Bypass Paywalls

/tncms/access/rules/*$xhr,domain=missoulian.com|nwitimes.com|tdn.com
||tinypass.com^$domain=bangordailynews.com|businessinsider.com|dig-in.com|fortune.com

||meter.bostonglobe.com/js/meter.js$script,1p
||metering.platform.sandiegouniontribune.com^$xhr,1p
||metering.platform.latimes.com^$xhr,1p
||stcatharinesstandard.ca/api/overlaydata$xhr,1p
||theglobeandmail.com/pf/dist/engine/react.js$script,1p
||washingtonpost.com/pwapiv2/article$xhr,1p
||zephr.orlandosentinel.com^$xhr,1p
||zephr.sun-sentinel.com^$xhr,1p
chicagobusiness.com##+js(json-prune, porte)
enotes.com##+js(rc, is-paywalled)
seattletimes.com##+js(aopr, SEATIMESCO.paywall)
theatlantic.com###paywall
theatlantic.com##body:style(overflow: auto !important)

! nytimes
! ||samizdat-graphql.nytimes.com/graphql/*$xhr,1p
||meter-svc.nytimes.com/meter.js$xhr,1p

! time.com
||time.com/dist/meter-wall-client-js.*.js$script,1p
time.com##.meter-wall
time.com##body:style(position:static!important;overflow-y:visible!important)
