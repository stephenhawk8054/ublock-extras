/// facebook_sponsor.js
(function() {
    (new MutationObserver(function() {
        document.querySelectorAll('div[data-pagelet^="FeedUnit"]:not([style*="display: none"])').forEach(function(a) {
            Object.keys(a).forEach(function(b) {
                if (b.includes("__reactProps")) {
                    b = a[b];
                    try {
                        if (b.children.props.children.props.edge.category.includes("SPONSORED")) {
                            a.style = "display: none !important;";
                        }
                    } catch (e) {}
                }
            })
        })
    })).observe(document, {
        childList: !0,
        subtree: !0
    })
})();

/// facebook_feed.js
(function() {
    const magic = String.fromCharCode(Date.now() % 26 + 97) +
                  Math.floor(Math.random() * 982451653 + 982451653).toString(36);
    const processInsertedFeedUnit = (feedUnit) => {
            const keys = Object.keys(feedUnit).filter(key => key.startsWith('__reactProps'));
            if ( keys.length != 1 ) { return; }
            const key = keys[0];
            try {
                // https://github.com/uBlockOrigin/uAssets/issues/3367#issuecomment-939415958
                // category: "ORGANIC" indicates regular feedunits
                // category: "ENGAGEMENT" indicates suggested feedunits
                // category: "SPONSORED" indicates sponsored feedunits
                if ( feedUnit[key].children.props.children.props.edge.category === 'SPONSORED' ) {
                    feedUnit.classList.add(magic);
                }
            } catch(e) {}
    };
    const start = ( ) => {
        const style = document.createElement('style');
        style.innerHTML = `
            .${magic} {
                display: none !important;
            }
        `;
        document.head.appendChild(style);
        const observer = new MutationObserver(mutations => {
            mutations.forEach((mutation) => {
                mutation.addedNodes.forEach((node) => {
                    if ( node instanceof HTMLDivElement ) {
                        const attr = node.getAttribute('data-pagelet');
                        if ( attr && /^FeedUnit/.test(attr) ) {
                            processInsertedFeedUnit(node);
                        }
                    }
                });
            });
        });
        observer.observe(document.body, { childList: true, subtree: true });
    };
    if ( document.readyState === 'loading' ) {
        self.addEventListener('DOMContentLoaded', start, { once: true });
    } else {
        start();
    }
})();
